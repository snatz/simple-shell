A simple shell implementation in C.

Usage:

```
make
```

```
xterm $PWD/sh
```

Compiled on gcc 5.4.0 on Ubuntu 16.4 x64.
