all: sh

sh: sh.o
	gcc -o sh sh.o

sh.o: sh.c
	gcc -o sh.o -c sh.c

clean:
	rm -rf *.o

mrproper: clean
	rm -rf sh
