#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

// Simplifed xv6 shell.

#define MAXARGS 10

// All commands have at least a type. Have looked at the type, the code
// typically casts the *cmd to some specific cmd type.
struct cmd {
    int type;          //  ' ' (exec), | (pipe), '<' or '>' for redirection, '&' for background
};

struct execcmd {
    int type;              // ' '
    char *argv[MAXARGS];   // arguments to the command to be exec-ed
};

struct redircmd {
    int type;          // < or > 
    struct cmd *cmd;   // the command to be run (e.g., an execcmd)
    char *file;        // the input/output file
    int mode;          // the mode to open the file with
    int fd;            // the file descriptor number to use for the file
};

struct pipecmd {
    int type;          // |
    struct cmd *left;  // left side of pipe
    struct cmd *right; // right side of pipe
};

struct bgcmd {			// background cmd
	int type;			// '&'
	struct cmd *cmd;
};

struct pidtab {
    int size;
    int array[200];
};

int fork1(void);  // Fork but exits on failure.
struct cmd *parsecmd(char*);
struct pidtab pids;
pid_t prog_pid;

// Execute cmd.  Never returns.
void runcmd(struct cmd *cmd) {
    struct execcmd *ecmd;
    struct pipecmd *pcmd;
    struct redircmd *rcmd;
    struct bgcmd *bcmd;

    if(cmd == 0)
        exit(0);

    switch(cmd->type) {
        default:
            fprintf(stderr, "unknown runcmd\n");
            exit(-1);

        case ' ':
            ecmd = (struct execcmd*)cmd;

            if(ecmd->argv[0] == 0)
                exit(0);

            execvp(ecmd->argv[0], ecmd->argv);

            break;

        case '&':
        	bcmd = (struct bgcmd*) cmd;
        	
        	runcmd(bcmd->cmd);

        	break;

        case '>':
            rcmd = (struct redircmd*)cmd;
            // manages output redirection
            rcmd->fd = open(rcmd->file, rcmd->mode);	
            dup2(rcmd->fd,1);
            runcmd(rcmd->cmd);
            break;

        case '<':
            rcmd = (struct redircmd*)cmd;
            // manages input redirection
            rcmd->fd = open(rcmd->file, rcmd->mode);
            dup2(rcmd->fd,0);
            runcmd(rcmd->cmd);
            break;

        case '|':
            pcmd = (struct pipecmd*)cmd;

            // manages pipes
            int filedes[2] = {0};
            if(pipe(filedes) == -1)
                fprintf(stderr, "Pipe error \n" );

            // Create first child
            switch(fork()) {
                case 0 :
                    if(close(filedes[0]) == -1)
                        fprintf(stderr, "Error : Close unused pipe \n" );
                    dup2(filedes[1], STDOUT_FILENO);
                    runcmd(pcmd->left);
                    fprintf(stderr, "Error : Exec left command \n" );
                    break;
                case -1:
                    fprintf(stderr, "Fork error \n" );
            }

            // Create second child
            switch(fork()) {
                case 0 :
                    if(close(filedes[1]) == -1)
                        fprintf(stderr, "Error : Close unused pipe \n" );
                    dup2(filedes[0],STDIN_FILENO);
                    runcmd(pcmd->right);
                    fprintf(stderr, "Error : Exec right command \n" );
                    break;

                case -1 :
                    fprintf(stderr, "Fork error \n" );
            }

            if(close(filedes[0]) || close(filedes[1]))
                fprintf(stderr, "Error : Parent close pipe \n" );

            wait(NULL);
            wait(NULL);
            break;
    }

    exit(0);
}

void newProcessGroup() {
    if(setpgid(0,0) < 0){
        perror("setpgid");
        exit(1);
    }
}

void putForeground(){
    if(tcsetpgrp(STDIN_FILENO, getpgrp()) < 0){
        perror("tcsetpgrp");
        exit(1);
    }
}

int findid(int *array, size_t size, int target) {
    for (int i = 0 ; i < size ; i++) {
        if (target == array[i]) {
            return i;
        }
    }
}

void signalHandler(int sig) {
    pid_t pid;
    int status;

    switch(sig){
        case SIGINT:
            if (getpid() != prog_pid) {
                kill(getpid(), sig);
            }
            break;
        
        case SIGHUP:
            for(int i=0;i<pids.size;i++){
                if(pids.array[i] != 0){
                    kill(pids.array[i], SIGHUP);
                    pids.array[i] = 0;
                }
            }

            pids.size = 0;
            exit(0);
            break;

        case SIGCHLD:
            while (pid = waitpid(-1, &status, WNOHANG) > 0) {
                int index = findid(pids.array, pids.size, pid);
                pids.array[index] = 0;
            }

            break;

        case SIGQUIT:
            exit(0);
            break;
    }
}

int getcmd(char *buf, int nbuf) {
    printf("$ ");
    memset(buf, 0, nbuf);
    fgets(buf, nbuf, stdin);

    if(buf[0] == 0) // EOF
        return -1;

    return 0;
}

int main(void) {
    static char buf[100];
    prog_pid = getpid();
    signal(SIGHUP, &signalHandler);
    signal(SIGINT, &signalHandler);
    signal(SIGQUIT, &signalHandler);
    signal(SIGCHLD, &signalHandler);

    pids.size = 0;

    // Read and run input commands.
    while(getcmd(buf, sizeof(buf)) >= 0) {      
        if(buf[0] == 'c' && buf[1] == 'd' && buf[2] == ' ') {
            buf[strlen(buf)-1] = 0;  // chop \n

            if(chdir(buf+3) < 0) {
                fprintf(stderr, "cannot cd %s\n", buf+3);
            }

            continue;
        } else {
            int isFG = 1;

            if (buf[strlen(buf)-2] == '&') {
                isFG = 0;
            }

            struct cmd* command = parsecmd(buf);
            int status;
            int pid = fork1();
            if(pid == 0){   //Child
                newProcessGroup();
                if(isFG == 1)
                    putForeground();
                else
                    printf("[PID] %i\n", getpid());
                runcmd(command);
            } else {   //Parent
                pids.array[pids.size++] = pid;
            }

            if(isFG) {   //wait that the command end and set the shell to FG process group
                if(waitpid(pid, &status, WUNTRACED) != -1) {
                    //delete from array
                    putForeground();
                }
            }
        }
    }

    exit(0);
}

int fork1(void) {
    int pid;

    pid = fork();
    if(pid == -1)
        perror("fork");

    return pid;
}

struct cmd* execcmd(void) {
    struct execcmd *cmd;

    cmd = malloc(sizeof(*cmd));
    memset(cmd, 0, sizeof(*cmd));
    cmd->type = ' ';

    return (struct cmd*)cmd;
}

struct cmd* redircmd(struct cmd *subcmd, char *file, int type) {
    struct redircmd *cmd;

    cmd = malloc(sizeof(*cmd));
    memset(cmd, 0, sizeof(*cmd));
    cmd->type = type;
    cmd->cmd = subcmd;
    cmd->file = file;
    cmd->mode = (type == '<') ?  O_RDONLY : O_WRONLY|O_CREAT|O_TRUNC;
    cmd->fd = (type == '<') ? 0 : 1;

    return (struct cmd*)cmd;
}

struct cmd* pipecmd(struct cmd *left, struct cmd *right) {
    struct pipecmd *cmd;

    cmd = malloc(sizeof(*cmd));
    memset(cmd, 0, sizeof(*cmd));
    cmd->type = '|';
    cmd->left = left;
    cmd->right = right;

    return (struct cmd*)cmd;
}

struct cmd* bgcmd(struct cmd *subcmd) {
	struct bgcmd *cmd;

	cmd = malloc(sizeof(*cmd));
	memset(cmd, 0, sizeof(*cmd));
	cmd->type = '&';
	cmd->cmd = subcmd;

	return (struct cmd*) cmd;
}

// Parsing
char whitespace[] = " \t\r\n\v";
char symbols[] = "<|>&";

int gettoken(char **ps, char *es, char **q, char **eq) {
    char *s;
    int ret;

    s = *ps;
    while(s < es && strchr(whitespace, *s))
        s++;
    if(q)
        *q = s;
    ret = *s;
    switch(*s){
        case 0:
            break;

        case '|':
        case '<':
        case '>':
        case '&':
        	s++;
        	break;

        default:
            ret = 'a';
            while(s < es && !strchr(whitespace, *s) && !strchr(symbols, *s))
                s++;
            break;
    }

    if(eq)
        *eq = s;

    while(s < es && strchr(whitespace, *s))
        s++;

    *ps = s;
    return ret;
}

// ps : pointer to a string
// es : pointer to the end of the string
// moves *ps to the next character, returns true if this character is in the
// string toks
int peek(char **ps, char *es, char *toks) {
    char *s;

    s = *ps;
    while(s < es && strchr(whitespace, *s))
        s++;

    *ps = s;
    return *s && strchr(toks, *s);
}

struct cmd *parseline(char**, char*);
struct cmd *parsepipe(char**, char*);
struct cmd *parseexec(char**, char*);

// make a copy of the characters in the input buffer, starting from s through es.
// null-terminate the copy to make it a string.
char *mkcopy(char *s, char *es) {
    int n = es - s;
    char *c = malloc(n+1);
    assert(c);
    strncpy(c, s, n);
    c[n] = 0;
    return c;
}

struct cmd* parsecmd(char *s) {
    char *es;
    struct cmd *cmd;

    es = s + strlen(s);
    cmd = parseline(&s, es);
    peek(&s, es, "");

    if(s != es){
        fprintf(stderr, "leftovers: %s\n", s);
        exit(-1);
    }

    return cmd;
}

struct cmd* parseline(char **ps, char *es) {
    struct cmd *cmd;
    cmd = parsepipe(ps, es);

    while (peek(ps, es, "&")) {
    	gettoken(ps, es, 0, 0);
    	cmd = bgcmd(cmd);
    }

    return cmd;
}

struct cmd* parsepipe(char **ps, char *es) {
    struct cmd *cmd;

    cmd = parseexec(ps, es);
    if(peek(ps, es, "|")){
        gettoken(ps, es, 0, 0);
        cmd = pipecmd(cmd, parsepipe(ps, es));
    }

    return cmd;
}

struct cmd* parseredirs(struct cmd *cmd, char **ps, char *es) {
    int tok;
    char *q, *eq;

    while(peek(ps, es, "<>")){
        tok = gettoken(ps, es, 0, 0);

        if(gettoken(ps, es, &q, &eq) != 'a') {
            fprintf(stderr, "missing file for redirection\n");
            exit(-1);
        }

        switch(tok) {
            case '<':
                cmd = redircmd(cmd, mkcopy(q, eq), '<');
                break;

            case '>':
                cmd = redircmd(cmd, mkcopy(q, eq), '>');
                break;
        }
    }

    return cmd;
}

struct cmd* parseexec(char **ps, char *es) {
    char *q, *eq;
    int tok, argc;
    struct execcmd *cmd;
    struct cmd *ret;

    ret = execcmd();
    cmd = (struct execcmd*)ret;

    argc = 0;
    ret = parseredirs(ret, ps, es);

    while(!peek(ps, es, "|&")) {
        if((tok=gettoken(ps, es, &q, &eq)) == 0)
            break;

        if(tok != 'a') {
            fprintf(stderr, "syntax error\n");
            exit(-1);
        }

        cmd->argv[argc] = mkcopy(q, eq);
        argc++;

        if(argc >= MAXARGS) {
            fprintf(stderr, "too many args\n");
            exit(-1);
        }
        
        ret = parseredirs(ret, ps, es);
    }

    cmd->argv[argc] = 0;
    return ret;
}